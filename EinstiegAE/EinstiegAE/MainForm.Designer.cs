﻿
namespace EinstiegAE
{
    partial class MainForm
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxbD = new System.Windows.Forms.TextBox();
            this.LblD = new System.Windows.Forms.Label();
            this.TxbR = new System.Windows.Forms.TextBox();
            this.TxbF = new System.Windows.Forms.TextBox();
            this.TxbU = new System.Windows.Forms.TextBox();
            this.LblR = new System.Windows.Forms.Label();
            this.LblF = new System.Windows.Forms.Label();
            this.LblU = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // TxbD
            // 
            this.TxbD.Location = new System.Drawing.Point(105, 12);
            this.TxbD.Name = "TxbD";
            this.TxbD.Size = new System.Drawing.Size(100, 20);
            this.TxbD.TabIndex = 0;
            this.TxbD.TextChanged += new System.EventHandler(this.TxbD_TextChanged);
            // 
            // LblD
            // 
            this.LblD.AutoSize = true;
            this.LblD.Location = new System.Drawing.Point(12, 15);
            this.LblD.Name = "LblD";
            this.LblD.Size = new System.Drawing.Size(69, 13);
            this.LblD.TabIndex = 1;
            this.LblD.Text = "Durchmesser";
            // 
            // TxbR
            // 
            this.TxbR.Location = new System.Drawing.Point(105, 39);
            this.TxbR.Name = "TxbR";
            this.TxbR.Size = new System.Drawing.Size(100, 20);
            this.TxbR.TabIndex = 2;
            this.TxbR.TextChanged += new System.EventHandler(this.TxbR_TextChanged);
            // 
            // TxbF
            // 
            this.TxbF.Location = new System.Drawing.Point(105, 66);
            this.TxbF.Name = "TxbF";
            this.TxbF.ReadOnly = true;
            this.TxbF.Size = new System.Drawing.Size(100, 20);
            this.TxbF.TabIndex = 3;
            // 
            // TxbU
            // 
            this.TxbU.Location = new System.Drawing.Point(105, 93);
            this.TxbU.Name = "TxbU";
            this.TxbU.ReadOnly = true;
            this.TxbU.Size = new System.Drawing.Size(100, 20);
            this.TxbU.TabIndex = 4;
            // 
            // LblR
            // 
            this.LblR.AutoSize = true;
            this.LblR.Location = new System.Drawing.Point(12, 42);
            this.LblR.Name = "LblR";
            this.LblR.Size = new System.Drawing.Size(40, 13);
            this.LblR.TabIndex = 5;
            this.LblR.Text = "Radius";
            // 
            // LblF
            // 
            this.LblF.AutoSize = true;
            this.LblF.Location = new System.Drawing.Point(12, 69);
            this.LblF.Name = "LblF";
            this.LblF.Size = new System.Drawing.Size(39, 13);
            this.LblF.TabIndex = 6;
            this.LblF.Text = "Fläche";
            // 
            // LblU
            // 
            this.LblU.AutoSize = true;
            this.LblU.Location = new System.Drawing.Point(12, 96);
            this.LblU.Name = "LblU";
            this.LblU.Size = new System.Drawing.Size(44, 13);
            this.LblU.TabIndex = 7;
            this.LblU.Text = "Umfang";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(214, 126);
            this.Controls.Add(this.LblU);
            this.Controls.Add(this.LblF);
            this.Controls.Add(this.LblR);
            this.Controls.Add(this.TxbU);
            this.Controls.Add(this.TxbF);
            this.Controls.Add(this.TxbR);
            this.Controls.Add(this.LblD);
            this.Controls.Add(this.TxbD);
            this.Name = "MainForm";
            this.Text = "Enstieg AE";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbD;
        private System.Windows.Forms.Label LblD;
        private System.Windows.Forms.TextBox TxbR;
        private System.Windows.Forms.TextBox TxbF;
        private System.Windows.Forms.TextBox TxbU;
        private System.Windows.Forms.Label LblR;
        private System.Windows.Forms.Label LblF;
        private System.Windows.Forms.Label LblU;
    }
}

