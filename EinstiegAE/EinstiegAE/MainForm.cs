﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EinstiegAE {
    public partial class MainForm : Form {
        public MainForm() {
            InitializeComponent();
        }

        private void Umfang(double d) {
            double u = Math.PI * d;
            TxbU.Text = u.ToString();
        }

        private void Flaeche(double r) {
            double A = Math.PI * Math.Pow(r, 2);
            TxbF.Text = A.ToString();
        }

        private void Berechne(double d, double r) {
            Umfang(d);
            Flaeche(r);
        }

        private void TxbD_TextChanged(object sender, EventArgs e)
        {
            double d;
            try {
                d = Convert.ToDouble(TxbD.Text);
            } catch (FormatException) {
                return;
            }
            double r = d / 2;
            TxbR.Text = r.ToString();
            Berechne(d, r);
        }

        private void TxbR_TextChanged(object sender, EventArgs e) {
            double r;
            try {
                r = Convert.ToDouble(TxbR.Text);
            }
            catch (FormatException) {
                return;
            }
            double d = r * 2;
            TxbD.Text = d.ToString();
            Berechne(d, r);
        }
    }
}
